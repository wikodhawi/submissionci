package com.dhabasoft.submissionjetpack.core.domain.repository

import com.dhabasoft.submissionjetpack.core.data.Resource
import com.dhabasoft.submissionjetpack.core.data.local.moviefavourite.MovieFavouriteEntity
import com.dhabasoft.submissionjetpack.core.data.local.tvshowfavourite.TvShowFavouriteEntity
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.movie.MovieResponse
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.moviedetail.MovieDetailResponse
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.tvshow.TvShowResponse
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.tvshowdetail.TvShowDetailResponse
import kotlinx.coroutines.flow.Flow

interface IMovieCatalogueRepository {
    fun getAllMovie(isFavourite: Boolean): Flow<Resource<MovieResponse>>
    fun getAllTvShow(isFavourite: Boolean): Flow<Resource<TvShowResponse>>

    fun getIsFavouriteMovie(movieId: Int)
    fun getFlowIsFavourite() : Flow<Boolean>
    fun setOrRemoveFromMovieFavourite(movieFavouriteEntity: MovieFavouriteEntity)

    fun getIsFavouriteTvShow(tvShowId: Int)
    fun getFlowIsFavouriteTvShow() : Flow<Boolean>
    fun setOrRemoveFromTvShowFavourite(tvShowFavouriteEntity: TvShowFavouriteEntity)

    fun getMovieDetail(movieId: Int): Flow<Resource<MovieDetailResponse>>
    fun getTvShowDetail(tvShowId: Int): Flow<Resource<TvShowDetailResponse>>
}