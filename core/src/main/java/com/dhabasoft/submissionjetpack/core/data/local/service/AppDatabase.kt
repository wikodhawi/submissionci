package com.dhabasoft.submissionjetpack.core.data.local.service

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.dhabasoft.submissionjetpack.core.data.local.moviefavourite.MovieFavouriteEntity
import com.dhabasoft.submissionjetpack.core.data.local.service.dao.MovieFavouriteDao
import com.dhabasoft.submissionjetpack.core.data.local.service.dao.TvShowFavouriteDao
import com.dhabasoft.submissionjetpack.core.data.local.tvshowfavourite.TvShowFavouriteEntity


@Database(
    entities =[MovieFavouriteEntity::class, TvShowFavouriteEntity::class], version = 1, exportSchema = false
)

@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun movieFavouriteDao(): MovieFavouriteDao
    abstract fun tvShowFavouriteDao(): TvShowFavouriteDao
}