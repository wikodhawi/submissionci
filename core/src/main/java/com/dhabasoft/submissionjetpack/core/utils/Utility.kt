package com.dhabasoft.submissionjetpack.core.utils

import com.dhabasoft.submissionjetpack.core.data.source.remote.response.moviedetail.Genre
import java.text.SimpleDateFormat
import java.util.*

object Utility {
    fun dateToReadableDate(dateString: String) : String {
        val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        return try {
            val date = sdf.parse(dateString)
            val sdfFormatted = SimpleDateFormat("d MMM yyyy", Locale.getDefault())
            sdfFormatted.format(date?: Date())
        } catch (e: Exception) {
            ""
        }
    }

    fun getGenres(genres: List<Genre>) : String {
        var result = ""
        for(i in genres.indices)
        {
            result += if(i == genres.size -1) {
                genres[i].name
            } else {
                "${genres[i].name}, "
            }
        }
        return result
    }

}