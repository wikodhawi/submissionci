package com.dhabasoft.submissionjetpack.core.data.source.local

import android.util.Log
import com.dhabasoft.submissionjetpack.core.data.local.service.dao.TvShowFavouriteDao
import com.dhabasoft.submissionjetpack.core.data.local.tvshowfavourite.TvShowFavouriteEntity
import com.dhabasoft.submissionjetpack.core.data.source.remote.network.ApiResponse
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.tvshow.TvShowResponse
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.tvshow.TvShowResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TvShowCatalogueLocalDataSource @Inject constructor(private val tvShowFavouriteDao: TvShowFavouriteDao,
) {
    fun getAllTvShow(): Flow<ApiResponse<TvShowResponse>> {
        return flow {
            try {
                val tvShowFavouriteEntity = tvShowFavouriteDao.getAllTvShow()
                val tvShowResults = parseTvShowFavouriteToTvShowResult(tvShowFavouriteEntity)
                val response = TvShowResponse(0, tvShowResults, 1, 1)
                emit(ApiResponse.Success(response))
            } catch (e : Exception){
                emit(ApiResponse.Error(e.toString()))
                Log.e("RemoteDataSource", e.toString())
            }
        }.flowOn(Dispatchers.IO)
    }

    val flowIsFavourite = MutableStateFlow(false)

    fun getIsFavouriteTvShow(tvShowId: Int) {
        val tvShowFavourite = tvShowFavouriteDao.getById(tvShowId)
        flowIsFavourite.value = tvShowFavourite != null
    }

    fun setOrRemoveFromTvShowFavourite(tvShowFavouriteEntity: TvShowFavouriteEntity) {
        val tvShowFavourite = tvShowFavouriteDao.getById(tvShowFavouriteEntity.id)
        if(tvShowFavourite == null)
            tvShowFavouriteDao.insertFavouriteTvShows(tvShowFavouriteEntity)
        else
            tvShowFavouriteDao.delete(tvShowFavouriteEntity.id)
        getIsFavouriteTvShow(tvShowFavouriteEntity.id)
    }

    private fun parseTvShowFavouriteToTvShowResult(listTvShowFavourite: List<TvShowFavouriteEntity>) : List<TvShowResult> {
        val result = mutableListOf<TvShowResult>()
        for(i in listTvShowFavourite) {
            result.add(TvShowResult(id = i.id, firstAirDate = i.firstAirDate, overview = i.overview, name =  i.name, posterPath = i.posterPath))
        }
        return result
    }
}