package com.dhabasoft.submissionjetpack.core.di

import android.content.Context
import androidx.room.Room
import androidx.room.RoomDatabase
import com.commonsware.cwac.saferoom.SafeHelperFactory
import com.dhabasoft.submissionjetpack.core.BuildConfig
import com.dhabasoft.submissionjetpack.core.data.local.service.AppDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import net.sqlcipher.database.SQLiteDatabase
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DatabaseModule {
    @Singleton
    @Provides
    fun provideSqlCipher(): SafeHelperFactory {
        val options = SafeHelperFactory.Options.builder().setClearPassphrase(false).build()
        val passphrase: ByteArray = SQLiteDatabase.getBytes(BuildConfig.DATABASE_PASSWORD.toCharArray())
        return SafeHelperFactory(passphrase, options)
    }

    @Provides
    fun provideAppDatabase(@ApplicationContext context: Context, supportFactory: SafeHelperFactory): AppDatabase =
        Room.databaseBuilder(context, AppDatabase::class.java, BuildConfig.DATABASE_NAME).allowMainThreadQueries().fallbackToDestructiveMigration().addCallback(
            object : RoomDatabase.Callback() {
            }).openHelperFactory(supportFactory).build()

    @Provides
    fun provideMovieFavouriteDao(appDatabase: AppDatabase) = appDatabase.movieFavouriteDao()

    @Provides
    fun provideTvShowFavouriteDao(appDatabase: AppDatabase) = appDatabase.tvShowFavouriteDao()


}