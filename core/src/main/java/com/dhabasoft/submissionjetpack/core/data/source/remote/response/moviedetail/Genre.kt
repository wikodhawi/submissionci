package com.dhabasoft.submissionjetpack.core.data.source.remote.response.moviedetail


import com.google.gson.annotations.SerializedName

data class Genre(
    @SerializedName("id")
    var id: Int?,
    @SerializedName("name")
    var name: String?
)