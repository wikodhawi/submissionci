package com.dhabasoft.submissionjetpack.core.data.local.moviefavourite

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity
class MovieFavouriteEntity (
        @PrimaryKey
        val id: Int,
        val title: String,
        val overview: String,
        val releaseDate: String,
        val posterPath: String,
        val createdDate: Date
        )