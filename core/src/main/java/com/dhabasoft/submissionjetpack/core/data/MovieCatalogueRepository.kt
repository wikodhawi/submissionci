package com.dhabasoft.submissionjetpack.core.data

import com.dhabasoft.submissionjetpack.core.data.local.moviefavourite.MovieFavouriteEntity
import com.dhabasoft.submissionjetpack.core.data.local.tvshowfavourite.TvShowFavouriteEntity
import com.dhabasoft.submissionjetpack.core.data.source.local.MovieCatalogueLocalDataSource
import com.dhabasoft.submissionjetpack.core.data.source.local.TvShowCatalogueLocalDataSource
import com.dhabasoft.submissionjetpack.core.data.source.remote.MovieCatalogueRemoteDataSource
import com.dhabasoft.submissionjetpack.core.data.source.remote.network.ApiResponse
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.movie.MovieResponse
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.moviedetail.MovieDetailResponse
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.tvshow.TvShowResponse
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.tvshowdetail.TvShowDetailResponse
import com.dhabasoft.submissionjetpack.core.domain.repository.IMovieCatalogueRepository
import com.dhabasoft.submissionjetpack.core.utils.EspressoIdlingResource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MovieCatalogueRepository @Inject constructor(
    private val remoteDataSource: MovieCatalogueRemoteDataSource,
    private val movieCatalogueLocalDataSource: MovieCatalogueLocalDataSource,
    private val tvShowCatalogueLocalDataSource: TvShowCatalogueLocalDataSource,
) : IMovieCatalogueRepository {

    override fun getIsFavouriteTvShow(tvShowId: Int) = tvShowCatalogueLocalDataSource.getIsFavouriteTvShow(tvShowId)

    override fun getFlowIsFavouriteTvShow(): Flow<Boolean> = tvShowCatalogueLocalDataSource.flowIsFavourite

    override fun setOrRemoveFromTvShowFavourite(tvShowFavouriteEntity: TvShowFavouriteEntity) = tvShowCatalogueLocalDataSource.setOrRemoveFromTvShowFavourite(tvShowFavouriteEntity)

    override fun getIsFavouriteMovie(movieId: Int) {
        movieCatalogueLocalDataSource.getIsFavouriteMovie(movieId)
    }

    override fun getFlowIsFavourite(): Flow<Boolean> {
        return movieCatalogueLocalDataSource.flowIsFavourite
    }

    override fun setOrRemoveFromMovieFavourite(movieFavouriteEntity: MovieFavouriteEntity) {
        movieCatalogueLocalDataSource.setOrRemoveFromMovieFavourite(movieFavouriteEntity)
    }

    override fun getAllMovie(isFavourite: Boolean): Flow<Resource<MovieResponse>> {
        if(isFavourite)
        {
            return flow {
                EspressoIdlingResource.increment()
                emit(Resource.Loading())
                when(val responseData = movieCatalogueLocalDataSource.getAllMovies().first())
                {
                    is ApiResponse.Success -> {
                        emit(Resource.Success(responseData.data))
                        EspressoIdlingResource.decrement()
                    }
                    is ApiResponse.Error -> {
                        emit(Resource.Error<MovieResponse>(responseData.errorMessage))
                        EspressoIdlingResource.decrement()
                    }
                }
            }
        }
        else
        {
            return flow {
                EspressoIdlingResource.increment()
                emit(Resource.Loading())
                when(val responseData = remoteDataSource.getAllMovies().first())
                {
                    is ApiResponse.Success -> {
                        emit(Resource.Success(responseData.data))
                        EspressoIdlingResource.decrement()
                    }
                    is ApiResponse.Error -> {
                        emit(Resource.Error<MovieResponse>(responseData.errorMessage))
                        EspressoIdlingResource.decrement()
                    }
                }
            }
        }
    }

    override fun getAllTvShow(isFavourite: Boolean): Flow<Resource<TvShowResponse>> {
        if(isFavourite)
        {
            return flow {
                EspressoIdlingResource.increment()
                emit(Resource.Loading())
                when(val responseData = tvShowCatalogueLocalDataSource.getAllTvShow().first())
                {
                    is ApiResponse.Success -> {
                        emit(Resource.Success(responseData.data))
                        EspressoIdlingResource.decrement()
                    }
                    is ApiResponse.Error -> {
                        emit(Resource.Error<TvShowResponse>(responseData.errorMessage))
                        EspressoIdlingResource.decrement()
                    }
                }
            }
        }
        else
        {
            return flow {
                EspressoIdlingResource.increment()
                emit(Resource.Loading())
                when(val responseData = remoteDataSource.getAllTvShow().first())
                {
                    is ApiResponse.Success -> {
                        emit(Resource.Success(responseData.data))
                        EspressoIdlingResource.decrement()
                    }
                    is ApiResponse.Error -> {
                        emit(Resource.Error<TvShowResponse>(responseData.errorMessage))
                        EspressoIdlingResource.decrement()
                    }
                }
            }
        }
    }

    override fun getMovieDetail(movieId: Int): Flow<Resource<MovieDetailResponse>> = flow {
        EspressoIdlingResource.increment()
        emit(Resource.Loading())
        when(val responseData = remoteDataSource.getMovieDetail(movieId).first())
        {
            is ApiResponse.Success -> {
                emit(Resource.Success(responseData.data))
                EspressoIdlingResource.decrement()
            }
            is ApiResponse.Error -> {
                emit(Resource.Error<MovieDetailResponse>(responseData.errorMessage))
                EspressoIdlingResource.decrement()
            }
        }
    }

    override fun getTvShowDetail(tvShowId: Int): Flow<Resource<TvShowDetailResponse>> = flow {
        EspressoIdlingResource.increment()
        emit(Resource.Loading())
        when(val responseData = remoteDataSource.getTvShowDetail(tvShowId).first())
        {
            is ApiResponse.Success -> {
                emit(Resource.Success(responseData.data))
                EspressoIdlingResource.decrement()
            }
            is ApiResponse.Error -> {
                emit(Resource.Error<TvShowDetailResponse>(responseData.errorMessage))
                EspressoIdlingResource.decrement()
            }
        }
    }
}