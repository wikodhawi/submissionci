package com.dhabasoft.submissionjetpack.core.data.source.remote.response.tvshowdetail


import com.google.gson.annotations.SerializedName

data class NextEpisodeToAir(
    @SerializedName("air_date")
    var airDate: String = "",
    @SerializedName("episode_number")
    var episodeNumber: Int = 0,
    @SerializedName("id")
    var id: Int = 0,
    @SerializedName("name")
    var name: String = "",
    @SerializedName("overview")
    var overview: String = "",
    @SerializedName("production_code")
    var productionCode: String = "",
    @SerializedName("season_number")
    var seasonNumber: Int = 0,
    @SerializedName("still_path")
    var stillPath: String = "",
    @SerializedName("vote_average")
    var voteAverage: Double = 0.0,
    @SerializedName("vote_count")
    var voteCount: Int = 0
)