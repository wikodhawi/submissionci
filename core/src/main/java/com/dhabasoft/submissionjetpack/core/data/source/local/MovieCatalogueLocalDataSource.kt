package com.dhabasoft.submissionjetpack.core.data.source.local

import android.util.Log
import com.dhabasoft.submissionjetpack.core.data.local.moviefavourite.MovieFavouriteEntity
import com.dhabasoft.submissionjetpack.core.data.local.service.dao.MovieFavouriteDao
import com.dhabasoft.submissionjetpack.core.data.source.remote.network.ApiResponse
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.movie.MovieResponse
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.movie.MovieResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MovieCatalogueLocalDataSource @Inject constructor(private val movieFavouriteDao: MovieFavouriteDao
) {

    fun getAllMovies(): Flow<ApiResponse<MovieResponse>> {
        return flow {
            try {
                val movieFavouriteEntity = movieFavouriteDao.getAllPaging()
                val movieResults = parseMovieFavouriteToMovieResult(movieFavouriteEntity)
                val response = MovieResponse(0, movieResults, 1, 1)
                emit(ApiResponse.Success(response))
            } catch (e : Exception){
                emit(ApiResponse.Error(e.toString()))
                Log.e("RemoteDataSource", e.toString())
            }
        }.flowOn(Dispatchers.IO)
    }

    val flowIsFavourite = MutableStateFlow(false)

    fun getIsFavouriteMovie(movieId: Int) {
        val movieFavourite = movieFavouriteDao.getById(movieId)
        flowIsFavourite.value = movieFavourite != null
    }

    fun setOrRemoveFromMovieFavourite(movieFavouriteEntity: MovieFavouriteEntity) {
        val movieFavourite = movieFavouriteDao.getById(movieFavouriteEntity.id)
        if(movieFavourite == null)
            movieFavouriteDao.insertFavouriteMovies(movieFavouriteEntity)
        else
            movieFavouriteDao.delete(movieFavouriteEntity.id)
        getIsFavouriteMovie(movieFavouriteEntity.id)
    }

    private fun parseMovieFavouriteToMovieResult(listMovieFavourite: List<MovieFavouriteEntity>) : List<MovieResult> {
        val result = mutableListOf<MovieResult>()
        for(i in listMovieFavourite) {
            result.add(MovieResult(id = i.id, releaseDate = i.releaseDate, overview = i.overview, title =  i.title, posterPath = i.posterPath))
        }
        return result
    }
}