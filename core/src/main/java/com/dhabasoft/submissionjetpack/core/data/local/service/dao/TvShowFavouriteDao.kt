package com.dhabasoft.submissionjetpack.core.data.local.service.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.dhabasoft.submissionjetpack.core.data.local.tvshowfavourite.TvShowFavouriteEntity

@Dao
interface TvShowFavouriteDao {
    @Query("SELECT * FROM TvShowFavouriteEntity order by createdDate")
    fun getAllTvShow(): List<TvShowFavouriteEntity>

    @Query("SELECT COUNT(*)/10+1 FROM TvShowFavouriteEntity")
    fun totalPage(): Int

    @Query("SELECT * FROM TvShowFavouriteEntity WHERE id=:id")
    fun getById(id: Int): TvShowFavouriteEntity?

    @Query("DELETE FROM TvShowFavouriteEntity WHERE id=:id")
    fun delete(id: Int)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertFavouriteTvShows(tvShowFavouriteEntity: TvShowFavouriteEntity)
}