package com.dhabasoft.submissionjetpack.core.utils

import com.dhabasoft.submissionjetpack.core.data.local.moviefavourite.MovieFavouriteEntity
import com.dhabasoft.submissionjetpack.core.data.local.tvshowfavourite.TvShowFavouriteEntity
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.movie.MovieResponse
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.moviedetail.MovieDetailResponse
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.tvshow.TvShowResponse
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.tvshowdetail.TvShowDetailResponse
import com.google.gson.Gson
import java.util.*

object DataDummy {
    private fun getStringFromFileJson(path: String): String {
        val bufferedReader = javaClass.getResourceAsStream(path)
        return bufferedReader?.bufferedReader()?.use { it.readText() } ?: ""
    }

    val dummyMovieFavouriteEntity = MovieFavouriteEntity(527774, "Raya and the Last Dragon", "Long ago, in the fantasy world of Kumandra, humans and dragons lived together in harmony. But when an evil force threatened the land, the dragons sacrificed themselves to save humanity. Now, 500 years later, that same evil has returned and it’s up to a lone warrior, Raya, to track down the legendary last dragon to restore the fractured land and its divided people.", "2021-03-03", "/lPsD10PP4rgUGiGR4CCXA6iY0QQ.jpg", Date())

    val dummyTvShowFavouriteEntity = TvShowFavouriteEntity(88396, "The Falcon and the Winter Soldier", "Sam and Bucky go to a criminal safe haven to find information about the Super Soldier serum.", "2021-04-03", "/6kbAMLteGO8yyewYau6bJ683sw7.jpg", Date())

    val generateMoviesResponse : MovieResponse = Gson().fromJson(getStringFromFileJson("/movies.json"), MovieResponse::class.java)

    val generateTvShows: TvShowResponse = Gson().fromJson(getStringFromFileJson("/tvshow.json"), TvShowResponse::class.java)

    val generateMovieDetail : MovieDetailResponse = Gson().fromJson("{\n" +
            "    \"adult\": false,\n" +
            "    \"backdrop_path\": \"/hJuDvwzS0SPlsE6MNFOpznQltDZ.jpg\",\n" +
            "    \"belongs_to_collection\": null,\n" +
            "    \"budget\": 0,\n" +
            "    \"genres\": [\n" +
            "        {\n" +
            "            \"id\": 16,\n" +
            "            \"name\": \"Animation\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"id\": 12,\n" +
            "            \"name\": \"Adventure\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"id\": 14,\n" +
            "            \"name\": \"Fantasy\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"id\": 10751,\n" +
            "            \"name\": \"Family\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"id\": 28,\n" +
            "            \"name\": \"Action\"\n" +
            "        }\n" +
            "    ],\n" +
            "    \"homepage\": \"https://movies.disney.com/raya-and-the-last-dragon\",\n" +
            "    \"id\": 527774,\n" +
            "    \"imdb_id\": \"tt5109280\",\n" +
            "    \"original_language\": \"en\",\n" +
            "    \"original_title\": \"Raya and the Last Dragon\",\n" +
            "    \"overview\": \"Long ago, in the fantasy world of Kumandra, humans and dragons lived together in harmony. But when an evil force threatened the land, the dragons sacrificed themselves to save humanity. Now, 500 years later, that same evil has returned and it’s up to a lone warrior, Raya, to track down the legendary last dragon to restore the fractured land and its divided people.\",\n" +
            "    \"popularity\": 4243.624,\n" +
            "    \"poster_path\": \"/lPsD10PP4rgUGiGR4CCXA6iY0QQ.jpg\",\n" +
            "    \"production_companies\": [\n" +
            "        {\n" +
            "            \"id\": 2,\n" +
            "            \"logo_path\": \"/wdrCwmRnLFJhEoH8GSfymY85KHT.png\",\n" +
            "            \"name\": \"Walt Disney Pictures\",\n" +
            "            \"origin_country\": \"US\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"id\": 6125,\n" +
            "            \"logo_path\": \"/tVPmo07IHhBs4HuilrcV0yujsZ9.png\",\n" +
            "            \"name\": \"Walt Disney Animation Studios\",\n" +
            "            \"origin_country\": \"US\"\n" +
            "        }\n" +
            "    ],\n" +
            "    \"production_countries\": [\n" +
            "        {\n" +
            "            \"iso_3166_1\": \"US\",\n" +
            "            \"name\": \"United States of America\"\n" +
            "        }\n" +
            "    ],\n" +
            "    \"release_date\": \"2021-03-03\",\n" +
            "    \"revenue\": 56482606,\n" +
            "    \"runtime\": 107,\n" +
            "    \"spoken_languages\": [\n" +
            "        {\n" +
            "            \"english_name\": \"English\",\n" +
            "            \"iso_639_1\": \"en\",\n" +
            "            \"name\": \"English\"\n" +
            "        }\n" +
            "    ],\n" +
            "    \"status\": \"Released\",\n" +
            "    \"tagline\": \"A quest to save her world.\",\n" +
            "    \"title\": \"Raya and the Last Dragon\",\n" +
            "    \"video\": false,\n" +
            "    \"vote_average\": 8.3,\n" +
            "    \"vote_count\": 1951\n" +
            "}", MovieDetailResponse::class.java)

    val generateTvShowDetail : TvShowDetailResponse = Gson().fromJson("{\n" +
            "    \"backdrop_path\": \"/JB17sIsU53NuWVUecOwrCA0CUp.jpg\",\n" +
            "    \"created_by\": [\n" +
            "        {\n" +
            "            \"id\": 1868712,\n" +
            "            \"credit_id\": \"605508e2960cde00721fc5e8\",\n" +
            "            \"name\": \"Malcolm Spellman\",\n" +
            "            \"gender\": 2,\n" +
            "            \"profile_path\": null\n" +
            "        }\n" +
            "    ],\n" +
            "    \"episode_run_time\": [\n" +
            "        50\n" +
            "    ],\n" +
            "    \"first_air_date\": \"2021-03-19\",\n" +
            "    \"genres\": [\n" +
            "        {\n" +
            "            \"id\": 10765,\n" +
            "            \"name\": \"Sci-Fi & Fantasy\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"id\": 10759,\n" +
            "            \"name\": \"Action & Adventure\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"id\": 18,\n" +
            "            \"name\": \"Drama\"\n" +
            "        }\n" +
            "    ],\n" +
            "    \"homepage\": \"https://www.disneyplus.com/series/the-falcon-and-the-winter-soldier/4gglDBMx8icA\",\n" +
            "    \"id\": 88396,\n" +
            "    \"in_production\": true,\n" +
            "    \"languages\": [\n" +
            "        \"en\"\n" +
            "    ],\n" +
            "    \"last_air_date\": \"2021-04-02\",\n" +
            "    \"last_episode_to_air\": {\n" +
            "        \"air_date\": \"2021-04-02\",\n" +
            "        \"episode_number\": 3,\n" +
            "        \"id\": 2535022,\n" +
            "        \"name\": \"Power Broker\",\n" +
            "        \"overview\": \"Sam and Bucky go to a criminal safe haven to find information about the Super Soldier serum.\",\n" +
            "        \"production_code\": \"\",\n" +
            "        \"season_number\": 1,\n" +
            "        \"still_path\": \"/hPUqRJmslgeuaTVKogf9eVtgNs9.jpg\",\n" +
            "        \"vote_average\": 5.7,\n" +
            "        \"vote_count\": 3\n" +
            "    },\n" +
            "    \"name\": \"The Falcon and the Winter Soldier\",\n" +
            "    \"next_episode_to_air\": {\n" +
            "        \"air_date\": \"2021-04-09\",\n" +
            "        \"episode_number\": 4,\n" +
            "        \"id\": 2558741,\n" +
            "        \"name\": \"\",\n" +
            "        \"overview\": \"\",\n" +
            "        \"production_code\": \"\",\n" +
            "        \"season_number\": 1,\n" +
            "        \"still_path\": null,\n" +
            "        \"vote_average\": 0.0,\n" +
            "        \"vote_count\": 0\n" +
            "    },\n" +
            "    \"networks\": [\n" +
            "        {\n" +
            "            \"name\": \"Disney+\",\n" +
            "            \"id\": 2739,\n" +
            "            \"logo_path\": \"/gJ8VX6JSu3ciXHuC2dDGAo2lvwM.png\",\n" +
            "            \"origin_country\": \"US\"\n" +
            "        }\n" +
            "    ],\n" +
            "    \"number_of_episodes\": 6,\n" +
            "    \"number_of_seasons\": 1,\n" +
            "    \"origin_country\": [\n" +
            "        \"US\"\n" +
            "    ],\n" +
            "    \"original_language\": \"en\",\n" +
            "    \"original_name\": \"The Falcon and the Winter Soldier\",\n" +
            "    \"overview\": \"Following the events of “Avengers: Endgame”, the Falcon, Sam Wilson and the Winter Soldier, Bucky Barnes team up in a global adventure that tests their abilities, and their patience.\",\n" +
            "    \"popularity\": 5223.212,\n" +
            "    \"poster_path\": \"/6kbAMLteGO8yyewYau6bJ683sw7.jpg\",\n" +
            "    \"production_companies\": [\n" +
            "        {\n" +
            "            \"id\": 420,\n" +
            "            \"logo_path\": \"/hUzeosd33nzE5MCNsZxCGEKTXaQ.png\",\n" +
            "            \"name\": \"Marvel Studios\",\n" +
            "            \"origin_country\": \"US\"\n" +
            "        }\n" +
            "    ],\n" +
            "    \"production_countries\": [\n" +
            "        {\n" +
            "            \"iso_3166_1\": \"US\",\n" +
            "            \"name\": \"United States of America\"\n" +
            "        }\n" +
            "    ],\n" +
            "    \"seasons\": [\n" +
            "        {\n" +
            "            \"air_date\": \"2021-03-19\",\n" +
            "            \"episode_count\": 6,\n" +
            "            \"id\": 156676,\n" +
            "            \"name\": \"Season 1\",\n" +
            "            \"overview\": \"\",\n" +
            "            \"poster_path\": \"/fIT6Y6O3cUX1X8qY8pZgzEvxUDQ.jpg\",\n" +
            "            \"season_number\": 1\n" +
            "        }\n" +
            "    ],\n" +
            "    \"spoken_languages\": [\n" +
            "        {\n" +
            "            \"english_name\": \"English\",\n" +
            "            \"iso_639_1\": \"en\",\n" +
            "            \"name\": \"English\"\n" +
            "        }\n" +
            "    ],\n" +
            "    \"status\": \"Returning Series\",\n" +
            "    \"tagline\": \"Honor the shield.\",\n" +
            "    \"type\": \"Miniseries\",\n" +
            "    \"vote_average\": 7.8,\n" +
            "    \"vote_count\": 2659\n" +
            "}", TvShowDetailResponse::class.java)
}