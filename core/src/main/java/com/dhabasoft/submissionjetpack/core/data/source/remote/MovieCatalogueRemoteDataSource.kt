package com.dhabasoft.submissionjetpack.core.data.source.remote

import android.util.Log
import com.dhabasoft.submissionjetpack.core.BuildConfig
import com.dhabasoft.submissionjetpack.core.data.source.remote.network.ApiResponse
import com.dhabasoft.submissionjetpack.core.data.source.remote.network.ApiService
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.movie.MovieResponse
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.moviedetail.MovieDetailResponse
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.tvshow.TvShowResponse
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.tvshowdetail.TvShowDetailResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MovieCatalogueRemoteDataSource @Inject constructor(private val apiService: ApiService
) {

    fun getAllMovies(): Flow<ApiResponse<MovieResponse>> {
        return flow {
            try {
                val response = apiService.getMovies(BuildConfig.API_KEY)
                emit(ApiResponse.Success(response))
            } catch (e : Exception){
                emit(ApiResponse.Error(e.toString()))
                Log.e("RemoteDataSource", e.toString())
            }
        }.flowOn(Dispatchers.IO)
    }

    fun getAllTvShow(): Flow<ApiResponse<TvShowResponse>> {
        return flow {
            try {
                val response = apiService.getTvShows(BuildConfig.API_KEY)
                emit(ApiResponse.Success(response))
            } catch (e : Exception){
                emit(ApiResponse.Error(e.toString()))
                Log.e("RemoteDataSource", e.toString())
            }
        }.flowOn(Dispatchers.IO)
    }

    fun getMovieDetail(movieId: Int): Flow<ApiResponse<MovieDetailResponse>> {
        return flow {
            try {
                val response = apiService.getMovieDetail(movieId, BuildConfig.API_KEY)
                emit(ApiResponse.Success(response))
            } catch (e : Exception){
                emit(ApiResponse.Error(e.toString()))
                Log.e("RemoteDataSource", e.toString())
            }
        }.flowOn(Dispatchers.IO)
    }

    fun getTvShowDetail(tvShowId: Int): Flow<ApiResponse<TvShowDetailResponse>> {
        return flow {
            try {
                val response = apiService.getTvShowDetail(tvShowId, BuildConfig.API_KEY)
                emit(ApiResponse.Success(response))
            } catch (e : Exception){
                emit(ApiResponse.Error(e.toString()))
                Log.e("RemoteDataSource", e.toString())
            }
        }.flowOn(Dispatchers.IO)
    }
}