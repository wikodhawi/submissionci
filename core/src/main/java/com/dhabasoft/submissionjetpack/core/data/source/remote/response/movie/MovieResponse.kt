package com.dhabasoft.submissionjetpack.core.data.source.remote.response.movie


import com.google.gson.annotations.SerializedName

data class MovieResponse(
        @SerializedName("page")
    var page: Int?,
        @SerializedName("results")
    var movieResults: List<MovieResult>?,
        @SerializedName("total_pages")
    var totalPages: Int?,
        @SerializedName("total_results")
    var totalResults: Int?
)