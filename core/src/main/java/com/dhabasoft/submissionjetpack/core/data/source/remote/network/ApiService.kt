package com.dhabasoft.submissionjetpack.core.data.source.remote.network

import com.dhabasoft.submissionjetpack.core.data.source.remote.response.movie.MovieResponse
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.moviedetail.MovieDetailResponse
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.tvshow.TvShowResponse
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.tvshowdetail.TvShowDetailResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {
    @GET("movie/popular")
    suspend fun getMovies(@Query("api_key") apiKey: String): MovieResponse

    @GET("tv/popular")
    suspend fun getTvShows(@Query("api_key") apiKey: String): TvShowResponse

    @GET("movie/{id}")
    suspend fun getMovieDetail(@Path("id") id: Int, @Query("api_key") apiKey: String): MovieDetailResponse

    @GET("tv/{id}")
    suspend fun getTvShowDetail(@Path("id") id: Int, @Query("api_key") apiKey: String): TvShowDetailResponse
}
