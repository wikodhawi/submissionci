package com.dhabasoft.submissionjetpack.core.domain.usecase

import com.dhabasoft.submissionjetpack.core.data.MovieCatalogueRepository
import com.dhabasoft.submissionjetpack.core.data.Resource
import com.dhabasoft.submissionjetpack.core.data.local.moviefavourite.MovieFavouriteEntity
import com.dhabasoft.submissionjetpack.core.data.local.tvshowfavourite.TvShowFavouriteEntity
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.movie.MovieResponse
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.moviedetail.MovieDetailResponse
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.tvshow.TvShowResponse
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.tvshowdetail.TvShowDetailResponse
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class MovieCatalogueInteractor @Inject constructor(private val movieCatalogueRepository: MovieCatalogueRepository): MovieUseCase {
    override fun getIsFavouriteMovie(movieId: Int) = movieCatalogueRepository.getIsFavouriteMovie(movieId)
    override fun getFlowIsFavourite(): Flow<Boolean> = movieCatalogueRepository.getFlowIsFavourite()
    override fun setOrRemoveFromMovieFavourite(movieFavouriteEntity: MovieFavouriteEntity) = movieCatalogueRepository.setOrRemoveFromMovieFavourite(movieFavouriteEntity)
    override fun getAllMovie(isFavourite: Boolean): Flow<Resource<MovieResponse>> = movieCatalogueRepository.getAllMovie(isFavourite)
    override fun getAllTvShow(isFavourite: Boolean): Flow<Resource<TvShowResponse>> = movieCatalogueRepository.getAllTvShow(isFavourite)
    override fun getMovieDetail(movieId: Int): Flow<Resource<MovieDetailResponse>> = movieCatalogueRepository.getMovieDetail(movieId)
    override fun getTvShowDetail(tvShowId: Int): Flow<Resource<TvShowDetailResponse>> = movieCatalogueRepository.getTvShowDetail(tvShowId)
    override fun getIsFavouriteTvShow(tvShowId: Int) = movieCatalogueRepository.getIsFavouriteTvShow(tvShowId)
    override fun getFlowIsFavouriteTvShow(): Flow<Boolean> = movieCatalogueRepository.getFlowIsFavouriteTvShow()
    override fun setOrRemoveFromTvShowFavourite(tvShowFavouriteEntity: TvShowFavouriteEntity) = movieCatalogueRepository.setOrRemoveFromTvShowFavourite(tvShowFavouriteEntity)
}