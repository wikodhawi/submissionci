package com.dhabasoft.submissionjetpack.core.data.local.service.dao

import androidx.room.*
import com.dhabasoft.submissionjetpack.core.data.local.moviefavourite.MovieFavouriteEntity

@Dao
interface MovieFavouriteDao {
    @Query("SELECT * FROM MovieFavouriteEntity order by createdDate")
    fun getAllPaging(): List<MovieFavouriteEntity>

    @Query("SELECT COUNT(*)/10+1 FROM MovieFavouriteEntity")
    fun totalPage(): Int

    @Query("SELECT * FROM MovieFavouriteEntity WHERE id=:id")
    fun getById(id: Int): MovieFavouriteEntity?

    @Query("DELETE FROM MovieFavouriteEntity WHERE id=:id")
    fun delete(id: Int)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertFavouriteMovies(movieFavouriteEntity: MovieFavouriteEntity)
}