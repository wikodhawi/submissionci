package com.dhabasoft.submissionjetpack.core.data.local.tvshowfavourite

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity
data class TvShowFavouriteEntity (@PrimaryKey
    val id: Int,
    val name: String,
    val overview: String,
    val firstAirDate: String,
      val posterPath: String,
    val createdDate: Date,

)