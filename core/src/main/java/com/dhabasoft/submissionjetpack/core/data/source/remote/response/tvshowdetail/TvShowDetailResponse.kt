package com.dhabasoft.submissionjetpack.core.data.source.remote.response.tvshowdetail


import com.dhabasoft.submissionjetpack.core.data.source.remote.response.moviedetail.Genre
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.moviedetail.ProductionCompany
import com.google.gson.annotations.SerializedName

data class TvShowDetailResponse(
    @SerializedName("backdrop_path")
    var backdropPath: String = "",
    @SerializedName("created_by")
    var createdBy: List<CreatedBy> = listOf(),
    @SerializedName("episode_run_time")
    var episodeRunTime: List<Int> = listOf(),
    @SerializedName("first_air_date")
    var firstAirDate: String = "",
    @SerializedName("genres")
    var genres: List<Genre> = listOf(),
    @SerializedName("homepage")
    var homepage: String = "",
    @SerializedName("id")
    var id: Int = 0,
    @SerializedName("in_production")
    var inProduction: Boolean = false,
    @SerializedName("languages")
    var languages: List<String> = listOf(),
    @SerializedName("last_air_date")
    var lastAirDate: String = "",
    @SerializedName("last_episode_to_air")
    var lastEpisodeToAir: LastEpisodeToAir = LastEpisodeToAir(),
    @SerializedName("name")
    var name: String = "",
    @SerializedName("networks")
    var networks: List<Network> = listOf(),
    @SerializedName("next_episode_to_air")
    var nextEpisodeToAir: NextEpisodeToAir = NextEpisodeToAir(),
    @SerializedName("number_of_episodes")
    var numberOfEpisodes: Int = 0,
    @SerializedName("number_of_seasons")
    var numberOfSeasons: Int = 0,
    @SerializedName("origin_country")
    var originCountry: List<String> = listOf(),
    @SerializedName("original_language")
    var originalLanguage: String = "",
    @SerializedName("original_name")
    var originalName: String = "",
    @SerializedName("overview")
    var overview: String = "",
    @SerializedName("popularity")
    var popularity: Double = 0.0,
    @SerializedName("poster_path")
    var posterPath: String = "",
    @SerializedName("production_companies")
    var productionCompanies: List<ProductionCompany> = listOf(),
    @SerializedName("production_countries")
    var productionCountries: List<ProductionCountry> = listOf(),
    @SerializedName("seasons")
    var seasons: List<Season> = listOf(),
    @SerializedName("spoken_languages")
    val spokenLanguages: List<SpokenLanguage> = listOf(),
    @SerializedName("status")
    var status: String = "",
    @SerializedName("tagline")
    var tagline: String = "",
    @SerializedName("type")
    var type: String = "",
    @SerializedName("vote_average")
    var voteAverage: Double = 0.0,
    @SerializedName("vote_count")
    var voteCount: Int = 0
)