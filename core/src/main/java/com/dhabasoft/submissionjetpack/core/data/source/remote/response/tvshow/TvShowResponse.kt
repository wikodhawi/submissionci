package com.dhabasoft.submissionjetpack.core.data.source.remote.response.tvshow


import com.google.gson.annotations.SerializedName

data class TvShowResponse(
        @SerializedName("page")
    var page: Int,
        @SerializedName("results")
    var results: List<TvShowResult>,
        @SerializedName("total_pages")
    var totalPages: Int,
        @SerializedName("total_results")
    var totalResults: Int
)