// Generated by Dagger (https://dagger.dev).
package com.dhabasoft.submissionjetpack.core.data.source.remote;

import com.dhabasoft.submissionjetpack.core.data.source.remote.network.ApiService;
import dagger.internal.Factory;
import javax.inject.Provider;

@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class MovieCatalogueRemoteDataSource_Factory implements Factory<MovieCatalogueRemoteDataSource> {
  private final Provider<ApiService> apiServiceProvider;

  public MovieCatalogueRemoteDataSource_Factory(Provider<ApiService> apiServiceProvider) {
    this.apiServiceProvider = apiServiceProvider;
  }

  @Override
  public MovieCatalogueRemoteDataSource get() {
    return newInstance(apiServiceProvider.get());
  }

  public static MovieCatalogueRemoteDataSource_Factory create(
      Provider<ApiService> apiServiceProvider) {
    return new MovieCatalogueRemoteDataSource_Factory(apiServiceProvider);
  }

  public static MovieCatalogueRemoteDataSource newInstance(ApiService apiService) {
    return new MovieCatalogueRemoteDataSource(apiService);
  }
}
