# Program Dicoding Belajar Jetpack Pro

## Submission 3

### Skenario test :
Unit Testing
- Pengujian terhadap class MovieCatalogueRepository pada method getAllMovie dari local dan remote, getAllTvShow  dari local dan remote, getMovieDetail, getTvShowDetail untuk memastikan apakah didapat sesuai dengan data yang diinginkan

- Pengujian terhadap class MovieViewModel pada method getMovies untuk memastikan apakah didapat sesuai dengan data yang diinginkan. 
    Berhubung Paging3 masih memiliki bug https://stackoverflow.com/questions/63522656/what-is-the-correct-way-to-check-the-data-from-a-pagingdata-object-in-android-un 
    mengambil list datanya maka pengecekan hanya terdapat pada value dari viewmodel apakah null atau tidak

- Pengujian terhadap class TvShowViewModel pada method getTvShows untuk memastikan apakah didapat sesuai dengan data yang diinginkan
    Berhubung Paging3 masih memiliki bug https://stackoverflow.com/questions/63522656/what-is-the-correct-way-to-check-the-data-from-a-pagingdata-object-in-android-un 
        mengambil list datanya maka pengecekan hanya terdapat pada value dari viewmodel apakah null atau tidak

- Pengujian terhadap class DetailMovieViewModel pada method getMovie untuk memastikan apakah data yang didapat sesuai dengan data yang diinginkan

- Pengujian terhadap class DetailTvShowViewModel pada method getMovie untuk memastikan apakah data yang didapat sesuai dengan data yang diinginkan
 

Instumentation Testing

Pengujian terhadap activity main untuk memastikan apakah:

1. Menampilkan data movies
    - Memastikan rcyMovies dalam keadaan tampil
    - Gulir rcyMovies ke posisi data terakhir halaman pertama yaitu 20
    - Gulir lagi rcyMovies ke posisi terakhir data halaman kedua yaitu 40
2. Menampilkan data detail movie
    - Memberikan tindakan klik pada data pertama rcyMovies
    - Memastikan TextView lblTitleMovieDetail dalam keadaan tampil
    - Memastikan TextView lblDurationDetailMovie dalam keadaan tampil 
	
3. Menampilkan data tv show
    - Memberikan tindakan pada TabLayout tabsDetail pada index 1
    - Memastikan rcyTvShows dalam keadaan tampil
    - Gulir rcyTvShows ke posisi data terakhir yaitu 20
    - Gulir lagi rcyTvShows ke posisi terakhir halaman 2 yaitu 40
4. Menampilkan data detail movie
    - Memberikan tindakan pada TabLayout tabsDetail pada index 1
    - Memberikan tindakan klik pada data pertama rcyTvShows
    - Memastikan TextView lblTitle dalam keadaan tampil
    - Memastikan TextView lblTotalEpisode dalam keadaan tampil
5. Menampilkan data movie favourite
    - Memberikan tindakan klik pada data index 1 rcyMovies
    - Memberikan tindakan klik pada fabFavouriteMovie
    - Memberikan tindakan back
    - Memberikan tindakan klik pada action_favourite
    - Memberikan tindakan klik pada data favourite movie index 0
    - Memberikan tindakan back
    - Memberikan tindakan back
    - Memberikan tindakan klik pada data index 0 rcyMovies
    - Memberikan tindakan klik pada fabFavouriteMovie
    - Memberikan tindakan back
    - Memberikan tindakan klik pada data favourite movie index 1
    - Memberikan tindakan klik pada fabFavouriteMovie untuk unfavourite
    - Memberikan tindakan back
    - Memberikan tindakan klik pada data favourite movie index 0
    - Memberikan tindakan klik pada fabFavouriteMovie untuk unfavourite
    - Memberikan tindakan back
    - Cek apakah rcyMovies tidak terlihat
    
6. Menampilkan data tv show favourite
    - Memberikan tindakan pada TabLayout tabsDetail pada index 1
    - Memberikan tindakan klik pada index 1 rcyTvShows
    - Memberikan tindakan klik pada fabFavourite
    - Memberikan tindakan back
    - Memberikan tindakan klik pada action_favourite
    - Memberikan tindakan klik TabLayout tabsDetail pada index 1
    - Gulir rcyTvShows ke index 0
    - Memberikan tindakan klik pada index 0 rcyTvShows
    - Memberikan tindakan back 
    - Memberikan tindakan back
    - Memberikan tindakan klik pada rcyTvShows index 1
    - Memberikan tindakan klik pada fabFavourite
    - Memberikan tindakan back
    - Memberikan tindakan klik pada action_favourite
    - Memberikan tindakan klik TabLayout tabsDetail pada index 1
    - Memberikan tindakan klik pada rcyTvShows index 1
    - Memberikan tindakan klik pada fabFavourite
    - Memberikan tindakan back
    - Memberikan tindakan klik pada rcyTvShows index 0
    - Memberikan tindakan klik pada fabFavourite
    - Memberikan tindakan back
    - Cek apakah rcyTvShows tidak terlihat
    
    
Project CI:
    - https://gitlab.com/wikodhawi/submissionci
    
Teknik Certificate Pinning menggunakan TrustManager & SSLSocketFactory:
    - core -> res -> raw -> mycertificate.cer
    - core -> di -> NetworkModule -> provideOkHttpClient 