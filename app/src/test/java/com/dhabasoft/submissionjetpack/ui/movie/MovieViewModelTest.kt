package com.dhabasoft.submissionjetpack.ui.movie

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.dhabasoft.submissionjetpack.core.data.MovieCatalogueRepository
import com.dhabasoft.submissionjetpack.core.data.Resource
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.movie.MovieResponse
import com.dhabasoft.submissionjetpack.core.utils.DataDummy
import com.dhabasoft.submissionjetpack.view.main.movies.MoviesViewModel
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class MovieViewModelTest {
    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var viewModel: MoviesViewModel

    private val repository = Mockito.mock(MovieCatalogueRepository::class.java)
    private val movieUseCase = FakeMovieCatalogInteractor(repository)

    companion object {
        val dataMovies = Resource.Success(
                DataDummy.generateMoviesResponse
        )
        val dummyFlowAllMovie = flow { emit(dataMovies)  }
    }

    @Mock
    private lateinit var observer: Observer<Resource<MovieResponse>>

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)
        viewModel = MoviesViewModel(movieUseCase)
    }

    @Test
    fun getMovies() = runBlocking {
        viewModel.movies.observeForever(observer)

        viewModel.getMovies(false)
        val movieEntities = viewModel.movies.value
        Assert.assertNotNull(movieEntities)
        Assert.assertEquals(20, movieEntities?.data?.movieResults?.size)

        Mockito.verify(observer).onChanged(dataMovies)
    }
}