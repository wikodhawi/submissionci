package com.dhabasoft.submissionjetpack.ui.movie

import com.dhabasoft.submissionjetpack.core.data.MovieCatalogueRepository
import com.dhabasoft.submissionjetpack.core.data.Resource
import com.dhabasoft.submissionjetpack.core.data.local.moviefavourite.MovieFavouriteEntity
import com.dhabasoft.submissionjetpack.core.data.local.tvshowfavourite.TvShowFavouriteEntity
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.movie.MovieResponse
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.moviedetail.MovieDetailResponse
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.tvshow.TvShowResponse
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.tvshowdetail.TvShowDetailResponse
import com.dhabasoft.submissionjetpack.core.domain.usecase.MovieUseCase
import com.dhabasoft.submissionjetpack.ui.detailmovie.DetailMovieViewModelTest
import com.dhabasoft.submissionjetpack.ui.detailtvshow.DetailTvShowViewModelTest
import com.dhabasoft.submissionjetpack.ui.tvshow.TvShowViewModelTest
import kotlinx.coroutines.flow.Flow

class FakeMovieCatalogInteractor(private val movieCatalogueRepository: MovieCatalogueRepository) :
    MovieUseCase {
    override fun getIsFavouriteMovie(movieId: Int) {
        movieCatalogueRepository.getIsFavouriteMovie(movieId)
    }

    override fun getFlowIsFavourite(): Flow<Boolean> = DetailMovieViewModelTest.dummyFlowIsFavourite

    override fun setOrRemoveFromMovieFavourite(movieFavouriteEntity: MovieFavouriteEntity) {
        movieCatalogueRepository.setOrRemoveFromMovieFavourite(movieFavouriteEntity)
    }

    override fun getIsFavouriteTvShow(tvShowId: Int) {
        movieCatalogueRepository.getIsFavouriteTvShow(tvShowId)
    }

    override fun getFlowIsFavouriteTvShow(): Flow<Boolean> = DetailTvShowViewModelTest.dummyFlowIsFavourite

    override fun setOrRemoveFromTvShowFavourite(tvShowFavouriteEntity: TvShowFavouriteEntity) {
        movieCatalogueRepository.setOrRemoveFromTvShowFavourite(tvShowFavouriteEntity)
    }

    override fun getAllMovie(isFavourite: Boolean): Flow<Resource<MovieResponse>> = MovieViewModelTest.dummyFlowAllMovie

    override fun getAllTvShow(isFavourite: Boolean): Flow<Resource<TvShowResponse>> = TvShowViewModelTest.dummyFlowAllTvShow

    override fun getMovieDetail(movieId: Int): Flow<Resource<MovieDetailResponse>> = DetailMovieViewModelTest.dummyFlowDetailMovie

    override fun getTvShowDetail(tvShowId: Int): Flow<Resource<TvShowDetailResponse>> = DetailTvShowViewModelTest.dummyFlowDetailTvShow
}