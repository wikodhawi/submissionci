package com.dhabasoft.submissionjetpack.ui.detailmovie

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.dhabasoft.submissionjetpack.core.data.MovieCatalogueRepository
import com.dhabasoft.submissionjetpack.core.data.Resource
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.moviedetail.MovieDetailResponse
import com.dhabasoft.submissionjetpack.core.utils.DataDummy
import com.dhabasoft.submissionjetpack.ui.movie.FakeMovieCatalogInteractor
import com.dhabasoft.submissionjetpack.view.detailmovie.DetailMovieViewModel
import kotlinx.coroutines.flow.flow
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class DetailMovieViewModelTest {
    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var viewModel: DetailMovieViewModel

    private val repository = Mockito.mock(MovieCatalogueRepository::class.java)

    private val movieUseCase = FakeMovieCatalogInteractor(repository)
    private val dummyMovieFavouriteEntity = DataDummy.dummyMovieFavouriteEntity

    companion object {
        val dataDetailMovie = Resource.Success(
            DataDummy.generateMovieDetail
        )
        val dummyFlowDetailMovie = flow<Resource<MovieDetailResponse>> { emit(dataDetailMovie) }
        const val dataIsFavourite = false
        val dummyFlowIsFavourite = flow { emit(dataIsFavourite) }
    }

    @Mock
    private lateinit var observer: Observer<Resource<MovieDetailResponse>>

    @Mock
    private lateinit var observer2: Observer<Boolean>

    @Before
    fun setUp() {
        viewModel = DetailMovieViewModel(movieUseCase)
    }

    @Test
    fun getMovieDetail() {
        val detailMovieLiveData = viewModel.getMovieDetail(1)

        detailMovieLiveData.observeForever(observer)
        viewModel.isFavourite.observeForever(observer2)
        val movieEntities = detailMovieLiveData.value
        Assert.assertNotNull(movieEntities)

        Mockito.verify(observer).onChanged(dataDetailMovie)
        Mockito.verify(observer2).onChanged(dataIsFavourite)

    }

    @Test
    fun setOrRemoveFavourite() {
        viewModel.setOrRemoveFromMovieFavourite(dummyMovieFavouriteEntity)
        Mockito.verify(repository).setOrRemoveFromMovieFavourite(dummyMovieFavouriteEntity)

        viewModel.getIsFavourite(dummyMovieFavouriteEntity.id)
        Mockito.verify(repository).getIsFavouriteMovie(dummyMovieFavouriteEntity.id)
    }
}