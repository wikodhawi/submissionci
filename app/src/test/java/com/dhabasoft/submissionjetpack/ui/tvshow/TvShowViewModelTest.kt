package com.dhabasoft.submissionjetpack.ui.tvshow

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.dhabasoft.submissionjetpack.core.data.MovieCatalogueRepository
import com.dhabasoft.submissionjetpack.core.data.Resource
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.tvshow.TvShowResponse
import com.dhabasoft.submissionjetpack.core.utils.DataDummy
import com.dhabasoft.submissionjetpack.ui.movie.FakeMovieCatalogInteractor
import com.dhabasoft.submissionjetpack.view.main.tvshows.TvShowsViewModel
import kotlinx.coroutines.flow.flow
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class TvShowViewModelTest {
    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var viewModel: TvShowsViewModel

    private val repository = Mockito.mock(MovieCatalogueRepository::class.java)
    private val movieUseCase = FakeMovieCatalogInteractor(repository)

    companion object {
        val dataTvShows = Resource.Success(
                DataDummy.generateTvShows
        )
        val dummyFlowAllTvShow = flow<Resource<TvShowResponse>> { emit(dataTvShows) }
    }

    @Mock
    private lateinit var observer: Observer<Resource<TvShowResponse>>

    @Before
    fun setUp() {
        viewModel = TvShowsViewModel(movieUseCase)
    }

    @Test
    fun getTvShows() {
        viewModel.tvShows.observeForever(observer)

        viewModel.getTvShows(false)
        val tvShowsEntities = viewModel.tvShows.value
        Assert.assertNotNull(tvShowsEntities)
        Assert.assertEquals(20, tvShowsEntities?.data?.results?.size)

        Mockito.verify(observer).onChanged(dataTvShows)
    }
}