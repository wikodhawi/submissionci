package com.dhabasoft.submissionjetpack.ui.detailtvshow

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.dhabasoft.submissionjetpack.core.data.MovieCatalogueRepository
import com.dhabasoft.submissionjetpack.core.data.Resource
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.tvshowdetail.TvShowDetailResponse
import com.dhabasoft.submissionjetpack.core.utils.DataDummy
import com.dhabasoft.submissionjetpack.ui.movie.FakeMovieCatalogInteractor
import com.dhabasoft.submissionjetpack.view.detailtvshow.DetailTvShowViewModel
import kotlinx.coroutines.flow.flow
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class DetailTvShowViewModelTest {
    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var viewModel: DetailTvShowViewModel

    private val repository = Mockito.mock(MovieCatalogueRepository::class.java)

    private val movieUseCase = FakeMovieCatalogInteractor(repository)

    private val dummyTvShowFavouriteEntity = DataDummy.dummyTvShowFavouriteEntity

    companion object {
        val dataDetailTvShow = Resource.Success(
            DataDummy.generateTvShowDetail
        )
        val dummyFlowDetailTvShow = flow<Resource<TvShowDetailResponse>> { emit(dataDetailTvShow) }
        const val dataIsFavourite = false
        val dummyFlowIsFavourite = flow { emit(dataIsFavourite) }
    }

    @Mock
    private lateinit var observer: Observer<Resource<TvShowDetailResponse>>

    @Mock
    private lateinit var observer2: Observer<Boolean>

    @Before
    fun setUp() {
        viewModel = DetailTvShowViewModel(movieUseCase)
    }

    @Test
    fun getTvShowDetail() {
        val detailTvShowLiveData = viewModel.getTvShowDetail(1)
        viewModel.isFavourite.observeForever(observer2)
        detailTvShowLiveData.observeForever(observer)
        val movieEntities = detailTvShowLiveData.value
        Assert.assertNotNull(movieEntities)

        Mockito.verify(observer).onChanged(dataDetailTvShow)
        Mockito.verify(observer2).onChanged(dataIsFavourite)
    }

    @Test
    fun setOrRemoveFavourite() {
        viewModel.setOrRemoveFromTvShowFavourite(dummyTvShowFavouriteEntity)
        Mockito.verify(repository).setOrRemoveFromTvShowFavourite(dummyTvShowFavouriteEntity)

        viewModel.getIsFavourite(dummyTvShowFavouriteEntity.id)
        Mockito.verify(repository).getIsFavouriteTvShow(dummyTvShowFavouriteEntity.id)
    }
}