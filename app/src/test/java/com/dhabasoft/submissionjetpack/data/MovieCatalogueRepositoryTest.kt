package com.dhabasoft.submissionjetpack.data

import com.dhabasoft.submissionjetpack.core.data.source.local.MovieCatalogueLocalDataSource
import com.dhabasoft.submissionjetpack.core.data.source.local.TvShowCatalogueLocalDataSource
import com.dhabasoft.submissionjetpack.core.data.source.remote.MovieCatalogueRemoteDataSource
import com.dhabasoft.submissionjetpack.core.data.source.remote.network.ApiResponse
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.movie.MovieResponse
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.moviedetail.MovieDetailResponse
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.tvshow.TvShowResponse
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.tvshowdetail.TvShowDetailResponse
import com.dhabasoft.submissionjetpack.core.utils.DataDummy
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.MockitoAnnotations


class MovieCatalogueRepositoryTest {
    private val movieCatalogueLocalDataSource = Mockito.mock(MovieCatalogueLocalDataSource::class.java)
    private val tvShowCatalogueLocalDataSource = Mockito.mock(TvShowCatalogueLocalDataSource::class.java)
    private val remoteDataSource = Mockito.mock(MovieCatalogueRemoteDataSource::class.java)
    private val repository = FakeMovieCatalogueRepository(remoteDataSource, movieCatalogueLocalDataSource, tvShowCatalogueLocalDataSource)
    private val dummyMovieFavouriteEntity = DataDummy.dummyMovieFavouriteEntity
    private val dummyTvShowFavouriteEntity = DataDummy.dummyTvShowFavouriteEntity

    private val moviesDataSourceResponse = flow<ApiResponse<MovieResponse>> {
        emit(ApiResponse.Success(DataDummy.generateMoviesResponse))
    }

    private val movieIsFavourite = MutableStateFlow(true)

    private val tvShowIsFavourite = MutableStateFlow(false)

    private val tvShowDataSourceResponse = flow<ApiResponse<TvShowResponse>> {
        emit(ApiResponse.Success(DataDummy.generateTvShows))
    }

    private val movieDetailDataSourceResponse = flow<ApiResponse<MovieDetailResponse>> {
        emit(ApiResponse.Success(DataDummy.generateMovieDetail))
    }

    private val tvShowDetailDataSourceResponse = flow<ApiResponse<TvShowDetailResponse>> {
        emit(ApiResponse.Success(DataDummy.generateTvShowDetail))
    }

    @Before
    @Throws(Exception::class)
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun `when get all movie from remote`() = runBlocking {
        Mockito.`when`(remoteDataSource.getAllMovies()).thenReturn(moviesDataSourceResponse)
        val moviesEntities = repository.getAllMovie(false).first()
        Mockito.verify(remoteDataSource).getAllMovies()
        Assert.assertNotNull(moviesEntities)
        assertEquals(20, moviesEntities.data?.movieResults?.size)
    }

    @Test
    fun `when get all movie from local`() = runBlocking {
        Mockito.`when`(movieCatalogueLocalDataSource.getAllMovies()).thenReturn(moviesDataSourceResponse)
        val moviesEntities = repository.getAllMovie(true).first()
        Mockito.verify(movieCatalogueLocalDataSource).getAllMovies()
        Assert.assertNotNull(moviesEntities)
        assertEquals(20, moviesEntities.data?.movieResults?.size)
    }

    @Test
    fun `when set or remove favourite movie`() = runBlocking {
        Mockito.`when`(movieCatalogueLocalDataSource.flowIsFavourite).thenReturn(movieIsFavourite)
        val isMovieFavourite = repository.getFlowIsFavourite().first()
        Assert.assertNotNull(isMovieFavourite)
        assertEquals(true, isMovieFavourite)

        repository.setOrRemoveFromMovieFavourite(dummyMovieFavouriteEntity)
        Mockito.verify(movieCatalogueLocalDataSource).setOrRemoveFromMovieFavourite(dummyMovieFavouriteEntity)

        repository.getIsFavouriteMovie(dummyMovieFavouriteEntity.id)
        Mockito.verify(movieCatalogueLocalDataSource).getIsFavouriteMovie(dummyMovieFavouriteEntity.id)
    }

    @Test
    fun `when get all tv show from remote`() = runBlocking {
        Mockito.`when`(remoteDataSource.getAllTvShow()).thenReturn(tvShowDataSourceResponse)
        val tvShowEntities = repository.getAllTvShow(false).first()
        Mockito.verify(remoteDataSource).getAllTvShow()
        Assert.assertNotNull(tvShowEntities)
        assertEquals(20, tvShowEntities.data?.results?.size)
    }

    @Test
    fun `when get all tv show from local`() = runBlocking {
        Mockito.`when`(tvShowCatalogueLocalDataSource.getAllTvShow()).thenReturn(tvShowDataSourceResponse)
        val tvShowEntities = repository.getAllTvShow(true).first()
        Mockito.verify(tvShowCatalogueLocalDataSource).getAllTvShow()
        Assert.assertNotNull(tvShowEntities)
        assertEquals(20, tvShowEntities.data?.results?.size)
    }

    @Test
    fun `when set or remove favourite tv show`() = runBlocking {
        Mockito.`when`(tvShowCatalogueLocalDataSource.flowIsFavourite).thenReturn(tvShowIsFavourite)
        val isTvShowFavourite = repository.getFlowIsFavouriteTvShow().first()
        Assert.assertNotNull(isTvShowFavourite)
        assertEquals(false, isTvShowFavourite)

        repository.setOrRemoveFromTvShowFavourite(dummyTvShowFavouriteEntity)
        Mockito.verify(tvShowCatalogueLocalDataSource).setOrRemoveFromTvShowFavourite(dummyTvShowFavouriteEntity)

        repository.getIsFavouriteTvShow(dummyTvShowFavouriteEntity.id)
        Mockito.verify(tvShowCatalogueLocalDataSource).getIsFavouriteTvShow(dummyTvShowFavouriteEntity.id)
    }

    @Test
    fun `when get movie detail`() = runBlocking {
        Mockito.`when`(remoteDataSource.getMovieDetail(1)).thenReturn(movieDetailDataSourceResponse)
        val movieDetailEntities = repository.getMovieDetail(1).first()
        Mockito.verify(remoteDataSource).getMovieDetail(1)
        Assert.assertNotNull(movieDetailEntities)
        assertEquals("Raya and the Last Dragon", movieDetailEntities.data?.originalTitle)
    }

    @Test
    fun `when get tv show detail`() = runBlocking {
        Mockito.`when`(remoteDataSource.getTvShowDetail(1)).thenReturn(tvShowDetailDataSourceResponse)
        val tvShowDetailEntities = repository.getTvShowDetail(1).first()
        Mockito.verify(remoteDataSource).getTvShowDetail(1)
        Assert.assertNotNull(tvShowDetailEntities)
        assertEquals("The Falcon and the Winter Soldier", tvShowDetailEntities.data?.name)
    }
}