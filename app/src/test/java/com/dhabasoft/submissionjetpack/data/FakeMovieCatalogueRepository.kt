package com.dhabasoft.submissionjetpack.data

import com.dhabasoft.submissionjetpack.core.data.Resource
import com.dhabasoft.submissionjetpack.core.data.local.moviefavourite.MovieFavouriteEntity
import com.dhabasoft.submissionjetpack.core.data.local.tvshowfavourite.TvShowFavouriteEntity
import com.dhabasoft.submissionjetpack.core.data.source.local.MovieCatalogueLocalDataSource
import com.dhabasoft.submissionjetpack.core.data.source.local.TvShowCatalogueLocalDataSource
import com.dhabasoft.submissionjetpack.core.data.source.remote.MovieCatalogueRemoteDataSource
import com.dhabasoft.submissionjetpack.core.data.source.remote.network.ApiResponse
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.movie.MovieResponse
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.moviedetail.MovieDetailResponse
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.tvshow.TvShowResponse
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.tvshowdetail.TvShowDetailResponse
import com.dhabasoft.submissionjetpack.core.domain.repository.IMovieCatalogueRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow

class FakeMovieCatalogueRepository (private val remoteDataSource: MovieCatalogueRemoteDataSource,
                                    private val movieCatalogueLocalDataSource: MovieCatalogueLocalDataSource,
                                    private val tvShowCatalogueLocalDataSource: TvShowCatalogueLocalDataSource
) : IMovieCatalogueRepository
{
    override fun getAllMovie(isFavourite: Boolean): Flow<Resource<MovieResponse>> {
        val allMovies =
        if(isFavourite)
        {
            movieCatalogueLocalDataSource.getAllMovies()
        }
        else
        {
            remoteDataSource.getAllMovies()
        }
        return flow()
        {
            emit(Resource.Success((allMovies.first() as ApiResponse.Success).data))
        }
    }

    override fun getAllTvShow(isFavourite: Boolean): Flow<Resource<TvShowResponse>> {
        val allTvShow =
                if(isFavourite)
                {
                    tvShowCatalogueLocalDataSource.getAllTvShow()
                }
                else
                {
                    remoteDataSource.getAllTvShow()
                }
        return flow()
        {
            emit(Resource.Success((allTvShow.first() as ApiResponse.Success).data))
        }
    }

    override fun getIsFavouriteMovie(movieId: Int) {
        movieCatalogueLocalDataSource.getIsFavouriteMovie(movieId)
    }

    override fun getFlowIsFavourite(): Flow<Boolean> {
        return movieCatalogueLocalDataSource.flowIsFavourite
    }

    override fun setOrRemoveFromMovieFavourite(movieFavouriteEntity: MovieFavouriteEntity) {
        movieCatalogueLocalDataSource.setOrRemoveFromMovieFavourite(movieFavouriteEntity)
    }

    override fun getIsFavouriteTvShow(tvShowId: Int) {
        tvShowCatalogueLocalDataSource.getIsFavouriteTvShow(tvShowId)
    }

    override fun getFlowIsFavouriteTvShow(): Flow<Boolean> {
        return tvShowCatalogueLocalDataSource.flowIsFavourite
    }

    override fun setOrRemoveFromTvShowFavourite(tvShowFavouriteEntity: TvShowFavouriteEntity) {
        tvShowCatalogueLocalDataSource.setOrRemoveFromTvShowFavourite(tvShowFavouriteEntity)
    }

    override fun getMovieDetail(movieId: Int): Flow<Resource<MovieDetailResponse>> {
        val movieDetail = remoteDataSource.getMovieDetail(movieId)
        return flow()
        {
            emit(Resource.Success((movieDetail.first() as ApiResponse.Success).data))
        }
    }

    override fun getTvShowDetail(tvShowId: Int): Flow<Resource<TvShowDetailResponse>> {
        val tvShowDetail = remoteDataSource.getTvShowDetail(tvShowId)
        return flow()
        {
            emit(Resource.Success((tvShowDetail.first() as ApiResponse.Success).data))
        }
    }
}