package com.dhabasoft.submissionjetpack.view.detailtvshow

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.dhabasoft.submissionjetpack.R
import com.dhabasoft.submissionjetpack.core.data.Resource
import com.dhabasoft.submissionjetpack.core.data.local.tvshowfavourite.TvShowFavouriteEntity
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.tvshowdetail.TvShowDetailResponse
import com.dhabasoft.submissionjetpack.databinding.ActivityDetailTvShowBinding
import com.dhabasoft.submissionjetpack.core.utils.EspressoIdlingResource
import com.dhabasoft.submissionjetpack.core.utils.Utility
import com.dhabasoft.submissionjetpack.view.main.movies.CompanyProductionAdapter
import dagger.hilt.android.AndroidEntryPoint
import java.util.*

@AndroidEntryPoint
class DetailTvShowActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDetailTvShowBinding
    private val viewModel: DetailTvShowViewModel by viewModels()

    companion object {
        const val TV_SHOW_SELECTED = "tvShowSelected"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailTvShowBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        val adapter = CompanyProductionAdapter()
        binding.rcyCompanyProduction.layoutManager = layoutManager
        binding.rcyCompanyProduction.adapter = adapter
        val tvShowId = intent.getIntExtra(TV_SHOW_SELECTED, 0)
        viewModel.getTvShowDetail(tvShowId).observe(this, { tvShowDetail->
            if(tvShowDetail != null)
            {
                when(tvShowDetail) {
                    is Resource.Loading -> {
                        binding.lblError.visibility = View.GONE
                        binding.lytMovieDetail .visibility = View.GONE
                        binding.lytLoading.root.visibility = View.VISIBLE
                    }
                    is Resource.Success -> {
                        binding.lblError.visibility = View.GONE
                        binding.lytMovieDetail.visibility = View.VISIBLE
                        binding.lytLoading.root.visibility = View.GONE
                        if(tvShowDetail.data != null)
                        {
                            adapter.setProductionCompany(tvShowDetail.data?.productionCompanies?: mutableListOf())
                            setDetailTvShow(tvShowDetail.data?: TvShowDetailResponse())
                        }
                    }
                    is Resource.Error -> {
                        binding.lblError.visibility = View.VISIBLE
                        binding.lblError.text = tvShowDetail.message
                        binding.lytLoading.root.visibility = View.GONE
                    }
                }
            }
        })
        viewModel.isFavourite.observe(this, {
            EspressoIdlingResource.increment()
            if(it)
            {
                binding.fabFavourite.setImageResource(R.drawable.ic_baseline_favorite_red)
            }
            else
            {
                binding.fabFavourite.setImageResource(R.drawable.ic_baseline_favorite_white)
            }
            EspressoIdlingResource.decrement()
        })
        viewModel.getIsFavourite(tvShowId)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)

    }

    private fun setDetailTvShow(tvShow: TvShowDetailResponse) {
        title = tvShow.name
        binding.lblTitle.text = tvShow.name
        binding.lblOriginalTitle.text = tvShow.originalName
        binding.lblDetailMovieSynopsis.text = tvShow.overview
        val totalEpisode = "${tvShow.numberOfEpisodes} ${getString(R.string.episode)}"
        binding.lblTotalEpisode.text =  totalEpisode
        binding.lblGenre.text = Utility.getGenres(tvShow.genres)
        binding.lblLanguage.text = tvShow.spokenLanguages[0].name
        val imageUrl = "${com.dhabasoft.submissionjetpack.BuildConfig.BASE_IMAGE_URL}/${tvShow.posterPath}"
        Glide.with(applicationContext).load(imageUrl).into(binding.imgDetailPoster)

        binding.fabFavourite.setOnClickListener {
            val tvShowFavouriteEntity = TvShowFavouriteEntity(id = tvShow.id,
                name = tvShow.name,
                overview = tvShow.overview,
                firstAirDate = tvShow.firstAirDate,
                createdDate = Date(),
                posterPath = tvShow.posterPath
            )
            viewModel.setOrRemoveFromTvShowFavourite(tvShowFavouriteEntity)
        }

    }
}