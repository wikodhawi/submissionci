package com.dhabasoft.submissionjetpack.view.main.movies

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.moviedetail.ProductionCompany
import com.dhabasoft.submissionjetpack.databinding.ItemProductionCompanyBinding

class CompanyProductionAdapter : RecyclerView.Adapter<CompanyProductionAdapter.ViewHolder>(){
    private var productionCompanies: List<ProductionCompany> = ArrayList()
    fun setProductionCompany(productionCompanies: List<ProductionCompany>)
    {
        this.productionCompanies = productionCompanies
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemProductionCompanyBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item: ProductionCompany = productionCompanies[position]
        holder.binding.lblCompanyName.text = item.name
        if(item.logoPath != null)
        {
            val imageUrl = "${com.dhabasoft.submissionjetpack.BuildConfig.BASE_IMAGE_URL}/${item.logoPath}"
            Glide.with(holder.itemView.context).load(imageUrl).into(holder.binding.imgPosterCompany)
        }
    }

    override fun getItemCount(): Int {
        return productionCompanies.size
    }

    class ViewHolder(val binding: ItemProductionCompanyBinding) : RecyclerView.ViewHolder(binding.root)
}