package com.dhabasoft.submissionjetpack.view.main.movies

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.movie.MovieResult
import com.dhabasoft.submissionjetpack.databinding.ItemMovieBinding
import com.dhabasoft.submissionjetpack.core.utils.Utility

class MoviesAdapter (private val onClickCallback: OnClickCallback) : RecyclerView.Adapter<MoviesAdapter.ViewHolder>(){
    private var movies: List<MovieResult> = ArrayList()
    fun setListMovies(movies: List<MovieResult>)
    {
        this.movies = movies
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemMovieBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item: MovieResult = movies[position]
        holder.binding.lblTitle.text = item.title
        val imageUrl = "${com.dhabasoft.submissionjetpack.BuildConfig.BASE_IMAGE_URL}/${item.posterPath}"
        Glide.with(holder.itemView.context).load(imageUrl).into(holder.binding.imgPoster)
        holder.binding.lblDescription.text = item.overview
        holder.binding.lblReleaseDate.text = Utility.dateToReadableDate(item.releaseDate)
        holder.binding.crdMovie.setOnClickListener {
            onClickCallback.toDetailMovie(item)
        }
    }

    override fun getItemCount(): Int {
        return movies.size
    }

    class ViewHolder(val binding: ItemMovieBinding) : RecyclerView.ViewHolder(binding.root)

    interface OnClickCallback {
        fun toDetailMovie(movieResult: MovieResult)
    }
}