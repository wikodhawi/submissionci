package com.dhabasoft.submissionjetpack.view.main.tvshows

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.tvshow.TvShowResult
import com.dhabasoft.submissionjetpack.databinding.ItemMovieBinding
import com.dhabasoft.submissionjetpack.core.utils.Utility

class TvShowsAdapter (private val onClickCallback: OnClickCallback) : RecyclerView.Adapter<TvShowsAdapter.ViewHolder>(){
    private var tvShows: List<TvShowResult> = ArrayList()
    fun setListMovies(tvShows: List<TvShowResult>)
    {
        this.tvShows = tvShows
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemMovieBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item: TvShowResult = tvShows[position]
        holder.binding.lblTitle.text = item.name
        val imageUrl = "${com.dhabasoft.submissionjetpack.BuildConfig.BASE_IMAGE_URL}/${item.posterPath}"
        Glide.with(holder.itemView.context).load(imageUrl).into(holder.binding.imgPoster)
        holder.binding.lblDescription.text = item.overview
        holder.binding.lblDescription.text = item.overview
        holder.binding.lblReleaseDate.text = Utility.dateToReadableDate(item.firstAirDate)
        holder.binding.crdMovie.setOnClickListener {
            onClickCallback.toDetailTvShow(item)
        }
    }

    override fun getItemCount(): Int {
        return tvShows.size
    }

    class ViewHolder(val binding: ItemMovieBinding) : RecyclerView.ViewHolder(binding.root)

    interface OnClickCallback {
        fun toDetailTvShow(tvShowResult: TvShowResult)
    }
}