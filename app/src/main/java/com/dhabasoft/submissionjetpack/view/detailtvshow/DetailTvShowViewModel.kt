package com.dhabasoft.submissionjetpack.view.detailtvshow

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.dhabasoft.submissionjetpack.core.data.Resource
import com.dhabasoft.submissionjetpack.core.data.local.tvshowfavourite.TvShowFavouriteEntity
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.tvshowdetail.TvShowDetailResponse
import com.dhabasoft.submissionjetpack.core.domain.usecase.MovieUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class DetailTvShowViewModel @Inject constructor(val movieUseCase: MovieUseCase) : ViewModel() {
    fun getTvShowDetail(tvShowId: Int): LiveData<Resource<TvShowDetailResponse>> = movieUseCase.getTvShowDetail(tvShowId).asLiveData()
    val isFavourite = movieUseCase.getFlowIsFavouriteTvShow().asLiveData()

    fun getIsFavourite(tvShowId: Int) = movieUseCase.getIsFavouriteTvShow(tvShowId)

    fun setOrRemoveFromTvShowFavourite(tvShowFavouriteEntity: TvShowFavouriteEntity) = movieUseCase.setOrRemoveFromTvShowFavourite(tvShowFavouriteEntity)
}