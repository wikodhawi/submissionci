package com.dhabasoft.submissionjetpack.view.main.tvshows

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.dhabasoft.submissionjetpack.core.data.Resource
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.tvshow.TvShowResult
import com.dhabasoft.submissionjetpack.databinding.FragmentTvShowsBinding
import com.dhabasoft.submissionjetpack.core.utils.EspressoIdlingResource
import com.dhabasoft.submissionjetpack.di.AppDependencies
import com.dhabasoft.submissionjetpack.di.DaggerAppComponent
import com.dhabasoft.submissionjetpack.di.ViewModelFactory
import com.dhabasoft.submissionjetpack.view.detailtvshow.DetailTvShowActivity
import dagger.hilt.android.EntryPointAccessors
import javax.inject.Inject

class TvShowsFragment : Fragment() {

    private lateinit var binding: FragmentTvShowsBinding
    @Inject
    lateinit var factory: ViewModelFactory

    private val viewModel: TvShowsViewModel by viewModels{factory}
    private lateinit var adapter: TvShowsAdapter
    private var isFavourite = false

    companion object {

        private const val IS_FAVOURITE = "isFavourite"

        @JvmStatic
        fun newInstance(isFavourite: Boolean) =
                TvShowsFragment().apply {
                    arguments = Bundle().apply {
                        putBoolean(IS_FAVOURITE, isFavourite)
                    }
                }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        DaggerAppComponent.builder()
            .context(requireActivity())
            .appDependencies(
                EntryPointAccessors.fromApplication(
                    requireContext(),
                    AppDependencies::class.java
                )
            )
            .build()
            .inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        binding = FragmentTvShowsBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(activity)
        EspressoIdlingResource.increment()
        isFavourite = arguments?.getBoolean(IS_FAVOURITE, false)?: false
        EspressoIdlingResource.decrement()
        binding.lytLoading.root.visibility = View.VISIBLE
        binding.rcyTvShows.apply {
            layoutManager = mLayoutManager
            itemAnimator = DefaultItemAnimator()
        }
        adapter = TvShowsAdapter(object : TvShowsAdapter.OnClickCallback{
            override fun toDetailTvShow(tvShowResult: TvShowResult) {
                val intent = Intent(activity, DetailTvShowActivity::class.java)
                intent.putExtra(DetailTvShowActivity.TV_SHOW_SELECTED, tvShowResult.id)
                startActivity(intent)
            }
        })
        binding.rcyTvShows.adapter = adapter
        viewModel.tvShows.observe(viewLifecycleOwner, { responseTvShow ->
            if(responseTvShow != null)
            {
                when(responseTvShow) {
                    is Resource.Loading -> {
                        binding.lblError.visibility = View.GONE
                        binding.rcyTvShows.visibility = View.GONE
                        binding.lytLoading.root.visibility = View.VISIBLE
                    }
                    is Resource.Success -> {
                        binding.lblError.visibility = View.GONE
                        binding.rcyTvShows.visibility = View.VISIBLE
                        binding.lytLoading.root.visibility = View.GONE
                        adapter.setListMovies(responseTvShow.data?.results?: mutableListOf())
                    }
                    is Resource.Error -> {
                        binding.lblError.visibility = View.VISIBLE
                        binding.lblError.text = responseTvShow.message
                        binding.rcyTvShows.visibility = View.GONE
                        binding.lytLoading.root.visibility = View.GONE
                    }
                }
            }
        })

        getTvShows()

    }

    fun getTvShows() {
        viewModel.getTvShows(isFavourite)
    }


}
