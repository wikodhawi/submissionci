package com.dhabasoft.submissionjetpack.view.detailmovie

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.dhabasoft.submissionjetpack.R
import com.dhabasoft.submissionjetpack.core.data.Resource
import com.dhabasoft.submissionjetpack.core.data.local.moviefavourite.MovieFavouriteEntity
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.moviedetail.MovieDetailResponse
import com.dhabasoft.submissionjetpack.databinding.ActivityDetailBinding
import com.dhabasoft.submissionjetpack.core.utils.EspressoIdlingResource
import com.dhabasoft.submissionjetpack.core.utils.Utility
import com.dhabasoft.submissionjetpack.view.main.movies.CompanyProductionAdapter
import dagger.hilt.android.AndroidEntryPoint
import java.util.*

@AndroidEntryPoint
class DetailMovieActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDetailBinding
    private val viewModel: DetailMovieViewModel by viewModels()

    companion object {
        const val MOVIE_SELECTED = "movieSelected"
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailBinding.inflate(layoutInflater)
        title=""
        setContentView(binding.root)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        val adapter = CompanyProductionAdapter()
        binding.rcyCompanyProduction.layoutManager = layoutManager
        binding.rcyCompanyProduction.adapter = adapter
        val movieId = intent.getIntExtra(MOVIE_SELECTED, 0)
        viewModel.getMovieDetail(movieId).observe(this, { movieDetailResponse->
            if(movieDetailResponse != null)
            {
                when(movieDetailResponse) {
                    is Resource.Loading -> {
                        binding.lblError.visibility = View.GONE
                        binding.lytMovieDetail .visibility = View.GONE
                        binding.lytLoading.root.visibility = View.VISIBLE
                    }
                    is Resource.Success -> {
                        binding.lblError.visibility = View.GONE
                        binding.lytMovieDetail.visibility = View.VISIBLE
                        binding.lytLoading.root.visibility = View.GONE
                        if(movieDetailResponse.data != null)
                        {
                            adapter.setProductionCompany(movieDetailResponse.data?.productionCompanies?: mutableListOf())
                            setDetailMovie(movieDetailResponse.data?: MovieDetailResponse())
                        }
                    }
                    is Resource.Error -> {
                        binding.lblError.visibility = View.VISIBLE
                        binding.lblError.text = movieDetailResponse.message
                        binding.lytLoading.root.visibility = View.GONE
                    }
                }
            }
        })
        viewModel.isFavourite.observe(this, {
            EspressoIdlingResource.increment()
            if(it)
            {
                binding.fabFavouriteMovie.setImageResource(R.drawable.ic_baseline_favorite_red)
            }
            else
            {
                binding.fabFavouriteMovie.setImageResource(R.drawable.ic_baseline_favorite_white)
            }
            EspressoIdlingResource.decrement()
        })
        viewModel.getIsFavourite(movieId)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)

    }

    private fun setDetailMovie(movie: MovieDetailResponse) {
        title = movie.title
        binding.lblTitleMovieDetail.text = movie.title
        binding.lblOriginalTitle.text = movie.originalTitle
        binding.lblDetailMovieSynopsis.text = movie.overview
        val duration = "${movie.runtime} ${getString(R.string.min)}"
        binding.lblDurationDetailMovie.text =  duration
        binding.lblGenre.text = Utility.getGenres(movie.genres)
        binding.lblLanguage.text = movie.spokenLanguages[0].name
        val imageUrl = "${com.dhabasoft.submissionjetpack.BuildConfig.BASE_IMAGE_URL}/${movie.posterPath}"
        Glide.with(applicationContext).load(imageUrl).into(binding.imgDetailPoster)

        binding.fabFavouriteMovie.setOnClickListener {
            val movieFavouriteEntity = MovieFavouriteEntity(id = movie.id,
                title = movie.title,
                overview = movie.overview,
                releaseDate = movie.releaseDate,
                createdDate = Date(),
                posterPath = movie.posterPath
            )
            viewModel.setOrRemoveFromMovieFavourite(movieFavouriteEntity)
        }
    }

}