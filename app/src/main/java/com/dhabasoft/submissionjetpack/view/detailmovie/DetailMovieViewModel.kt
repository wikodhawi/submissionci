package com.dhabasoft.submissionjetpack.view.detailmovie

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.dhabasoft.submissionjetpack.core.data.Resource
import com.dhabasoft.submissionjetpack.core.data.local.moviefavourite.MovieFavouriteEntity
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.moviedetail.MovieDetailResponse
import com.dhabasoft.submissionjetpack.core.domain.usecase.MovieUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class DetailMovieViewModel @Inject constructor(val movieUseCase: MovieUseCase) : ViewModel() {
    fun getMovieDetail(movieId: Int): LiveData<Resource<MovieDetailResponse>> = movieUseCase.getMovieDetail(movieId).asLiveData()

    val isFavourite = movieUseCase.getFlowIsFavourite().asLiveData()

    fun getIsFavourite(movieId: Int) = movieUseCase.getIsFavouriteMovie(movieId)

    fun setOrRemoveFromMovieFavourite(movieFavouriteEntity: MovieFavouriteEntity) = movieUseCase.setOrRemoveFromMovieFavourite(movieFavouriteEntity)
}