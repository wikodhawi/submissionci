package com.dhabasoft.submissionjetpack.view.main.movies

import androidx.lifecycle.*
import com.dhabasoft.submissionjetpack.core.data.Resource
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.movie.MovieResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MoviesViewModel @Inject constructor(private val movieUseCase: com.dhabasoft.submissionjetpack.core.domain.usecase.MovieUseCase) : ViewModel() {
    private val moviesMutableLiveData : MutableLiveData<Resource<MovieResponse>> = MutableLiveData()
    fun getMovies(isFavourite: Boolean) {
        movieUseCase.getAllMovie(isFavourite).asLiveData().observeForever {
            moviesMutableLiveData.postValue(it)
        }
    }

    val movies : LiveData<Resource<MovieResponse>> = moviesMutableLiveData
}

