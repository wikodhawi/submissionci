package com.dhabasoft.submissionjetpack.view.main

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.dhabasoft.submissionjetpack.view.main.movies.MoviesFragment
import com.dhabasoft.submissionjetpack.view.main.tvshows.TvShowsFragment

class SectionsPagerAdapter(activity: AppCompatActivity, private val isFavourite: Boolean = false) : FragmentStateAdapter(activity) {

    override fun createFragment(position: Int): Fragment {
        var fragment: Fragment? = null
        when (position) {
            0 -> fragment = MoviesFragment.newInstance(isFavourite)
            1 -> fragment = TvShowsFragment.newInstance(isFavourite)
        }
        return fragment as Fragment
    }

    override fun getItemCount(): Int {
        return 2
    }
}


