package com.dhabasoft.submissionjetpack.view.main.tvshows

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.dhabasoft.submissionjetpack.core.data.Resource
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.tvshow.TvShowResponse
import com.dhabasoft.submissionjetpack.core.domain.usecase.MovieUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class TvShowsViewModel @Inject constructor(val movieUseCase: MovieUseCase) : ViewModel() {

    private val tvShowMutableLiveData : MutableLiveData<Resource<TvShowResponse>> = MutableLiveData()
    fun getTvShows(isFavourite: Boolean) {
        movieUseCase.getAllTvShow(isFavourite).asLiveData().observeForever {
            tvShowMutableLiveData.postValue(it)
        }
    }

    val tvShows : LiveData<Resource<TvShowResponse>> = tvShowMutableLiveData

}