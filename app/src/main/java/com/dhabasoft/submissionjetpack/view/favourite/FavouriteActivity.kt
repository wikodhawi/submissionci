package com.dhabasoft.submissionjetpack.view.favourite

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.dhabasoft.submissionjetpack.R
import com.dhabasoft.submissionjetpack.databinding.ActivityMainBinding
import com.dhabasoft.submissionjetpack.view.main.SectionsPagerAdapter
import com.dhabasoft.submissionjetpack.view.main.movies.MoviesFragment
import com.dhabasoft.submissionjetpack.view.main.tvshows.TvShowsFragment
import com.google.android.material.tabs.TabLayoutMediator
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FavouriteActivity : AppCompatActivity() {
    companion object {
        private val TAB_TITLES = intArrayOf(
            R.string.movies,
            R.string.tv_shows
        )
    }

    private lateinit var binding : ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        setContentView(binding.root)
        title = getString(R.string.favourite_catalogue)
        setViewPager()
    }

    private fun setViewPager() {
        val sectionsPagerAdapter = SectionsPagerAdapter(this, true)
        binding.viewPagerDetail.adapter = sectionsPagerAdapter
        TabLayoutMediator(binding.tabsDetail, binding.viewPagerDetail) { tab, position ->
            tab.text = resources.getString(TAB_TITLES[position])
        }.attach()

        supportActionBar?.elevation = 0f
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onRestart() {
        super.onRestart()
        for(i in supportFragmentManager.fragments) {
            if(i is MoviesFragment)
                i.getMovies()
            else if(i is TvShowsFragment)
                i.getTvShows()
        }
    }
}