package com.dhabasoft.submissionjetpack.view.main.movies

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.dhabasoft.submissionjetpack.R
import com.dhabasoft.submissionjetpack.core.data.Resource
import com.dhabasoft.submissionjetpack.core.data.source.remote.response.movie.MovieResult
import com.dhabasoft.submissionjetpack.databinding.FragmentMoviesBinding
import com.dhabasoft.submissionjetpack.core.utils.EspressoIdlingResource
import com.dhabasoft.submissionjetpack.di.AppDependencies
import com.dhabasoft.submissionjetpack.di.DaggerAppComponent
import com.dhabasoft.submissionjetpack.di.ViewModelFactory
import com.dhabasoft.submissionjetpack.view.detailmovie.DetailMovieActivity
import dagger.hilt.android.EntryPointAccessors
import javax.inject.Inject

class MoviesFragment : Fragment() {

    private lateinit var binding: FragmentMoviesBinding
    private lateinit var adapter: MoviesAdapter

    @Inject
    lateinit var factory: ViewModelFactory

    private val viewModel: MoviesViewModel by viewModels {
        factory
    }
    private var isFavourite = false

    companion object {

        private const val IS_FAVOURITE = "isFavourite"

        @JvmStatic
        fun newInstance(isFavourite: Boolean) =
            MoviesFragment().apply {
                arguments = Bundle().apply {
                    putBoolean(IS_FAVOURITE, isFavourite)
                }
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        DaggerAppComponent.builder()
            .context(requireActivity())
            .appDependencies(
                EntryPointAccessors.fromApplication(
                    requireContext(),
                    AppDependencies::class.java
                )
            )
            .build()
            .inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        binding = FragmentMoviesBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(activity)

        EspressoIdlingResource.increment()
        isFavourite = arguments?.getBoolean(IS_FAVOURITE, false)?: false
        EspressoIdlingResource.decrement()
        binding.lytLoading.root.visibility = View.VISIBLE
        binding.rcyMovies.apply {
            layoutManager = mLayoutManager
            itemAnimator = DefaultItemAnimator()
        }
        adapter = MoviesAdapter(object : MoviesAdapter.OnClickCallback{
            override fun toDetailMovie(movieResult: MovieResult) {
                val intent = Intent(activity, DetailMovieActivity::class.java)
                intent.putExtra(DetailMovieActivity.MOVIE_SELECTED, movieResult.id)
                startActivity(intent)
            }
        })
        binding.rcyMovies.adapter = adapter
        viewModel.movies.observe(viewLifecycleOwner, { responseMovie ->
            if(responseMovie != null)
            {
                when(responseMovie) {
                    is Resource.Loading -> {
                        binding.lblError.visibility = View.GONE
                        binding.rcyMovies.visibility = View.GONE
                        binding.lytLoading.root.visibility = View.VISIBLE
                    }
                    is Resource.Success -> {
                        binding.rcyMovies.visibility = View.VISIBLE
                        binding.lytLoading.root.visibility = View.GONE
                        val listMovie = responseMovie.data?.movieResults ?: mutableListOf()
                        if (listMovie.isEmpty())
                        {
                            binding.lblError.visibility = View.VISIBLE
                            binding.lblError.text = getString(R.string.no_data)
                        }
                        else
                        {
                            binding.lblError.visibility = View.GONE
                        }
                        adapter.setListMovies(listMovie)
                    }
                    is Resource.Error -> {
                        binding.lblError.visibility = View.VISIBLE
                        binding.lblError.text = responseMovie.message
                        binding.rcyMovies.visibility = View.GONE
                        binding.lytLoading.root.visibility = View.GONE
                    }
                }
            }
        })
        getMovies()
    }

    fun getMovies() {
        EspressoIdlingResource.increment()
        viewModel.getMovies(isFavourite)
        EspressoIdlingResource.decrement()
    }
}
