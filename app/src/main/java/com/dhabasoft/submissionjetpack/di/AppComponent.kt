package com.dhabasoft.submissionjetpack.di

import android.app.Activity
import com.dhabasoft.submissionjetpack.view.main.MainActivity
import com.dhabasoft.submissionjetpack.view.main.movies.MoviesFragment
import com.dhabasoft.submissionjetpack.view.main.tvshows.TvShowsFragment
import dagger.BindsInstance
import dagger.Component

@Component(dependencies = [AppDependencies::class])
interface AppComponent{
    fun inject(activity: MainActivity)
    fun inject(moviesFragment: MoviesFragment)
    fun inject(tvShowsFragment: TvShowsFragment)

    @Component.Builder
    interface Builder {
        fun context(@BindsInstance activity: Activity): Builder
        fun appDependencies(appDependencies: AppDependencies): Builder
        fun build(): AppComponent
    }

}