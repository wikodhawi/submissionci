package com.dhabasoft.submissionjetpack.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.dhabasoft.submissionjetpack.core.domain.usecase.MovieUseCase
import com.dhabasoft.submissionjetpack.view.main.movies.MoviesViewModel
import com.dhabasoft.submissionjetpack.view.main.tvshows.TvShowsViewModel
import javax.inject.Inject

class ViewModelFactory @Inject constructor(private val movieUseCase: MovieUseCase) :
    ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T =
        when {
            modelClass.isAssignableFrom(MoviesViewModel::class.java) -> {
                MoviesViewModel(movieUseCase) as T
            }
            modelClass.isAssignableFrom(TvShowsViewModel::class.java) -> {
                TvShowsViewModel(movieUseCase) as T
            }
            else -> throw Throwable("Unknown ViewModel class: " + modelClass.name)
        }
}