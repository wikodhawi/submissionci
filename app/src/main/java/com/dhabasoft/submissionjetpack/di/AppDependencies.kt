package com.dhabasoft.submissionjetpack.di

import com.dhabasoft.submissionjetpack.core.domain.usecase.MovieUseCase
import dagger.hilt.EntryPoint
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@EntryPoint
@InstallIn(SingletonComponent::class)
interface AppDependencies {
    fun movieUseCase(): MovieUseCase
    fun viewModelFactory(): ViewModelFactory
}