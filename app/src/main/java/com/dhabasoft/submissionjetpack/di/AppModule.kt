package com.dhabasoft.submissionjetpack.di

import com.dhabasoft.submissionjetpack.core.domain.usecase.MovieCatalogueInteractor
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class AppModule {

    @Binds
    abstract fun provideMovieUseCase(movieCatalogueInteractor: MovieCatalogueInteractor): com.dhabasoft.submissionjetpack.core.domain.usecase.MovieUseCase

}
