package com.dhabasoft.submissionjetpack.main

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.*
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import com.dhabasoft.submissionjetpack.R
import com.dhabasoft.submissionjetpack.core.utils.EspressoIdlingResource
import com.dhabasoft.submissionjetpack.view.main.MainActivity
import com.google.android.material.tabs.TabLayout
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.CoreMatchers.not
import org.hamcrest.Matcher
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
@LargeTest
class MainActivityTest {
    @get:Rule
    var activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Before
    fun setUp() {
        ActivityScenario.launch(MainActivity::class.java)
        IdlingRegistry.getInstance().register(EspressoIdlingResource.idlingResource)
    }

    @After
    fun tearDown() {
        IdlingRegistry.getInstance().unregister(EspressoIdlingResource.idlingResource)
    }

    @Test
    fun assertMovies() {
        Espresso.onView(withId(R.id.rcyMovies)).check(ViewAssertions.matches(isDisplayed()))
        Espresso.onView(withId(R.id.rcyMovies)).perform(
            RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(
                20
            )
        )
    }

    @Test
    fun assertDetailMovie() {
        Espresso.onView(withId(R.id.rcyMovies)).perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                0,
                click()
            )
        )
        Espresso.onView(withId(R.id.lblTitleMovieDetail)).check(ViewAssertions.matches(isDisplayed()))
        Espresso.onView(withId(R.id.lblDurationDetailMovie)).check(
            ViewAssertions.matches(
                isDisplayed()
            )
        )
    }

    private fun selectTabAtPosition(tabIndex: Int): ViewAction {
        return object : ViewAction {
            override fun getDescription() = "with tab at index $tabIndex"

            override fun getConstraints() = allOf(
                isDisplayed(),
                isAssignableFrom(TabLayout::class.java)
            )

            override fun perform(uiController: UiController, view: View) {
                val tabLayout = view as TabLayout
                val tabAtIndex: TabLayout.Tab = tabLayout.getTabAt(tabIndex)
                        ?: throw PerformException.Builder()
                                .withCause(Throwable("No tab at index $tabIndex"))
                                .build()

                tabAtIndex.select()
            }
        }
    }

    @Test
    fun assertTvShows() {
        Espresso.onView(withId(R.id.tabsDetail)).perform(selectTabAtPosition(1))
        Espresso.onView(withId(R.id.rcyTvShows)).check(ViewAssertions.matches(isDisplayed()))
        Espresso.onView(withId(R.id.rcyTvShows)).perform(
            RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(
                20
            )
        )
    }

    @Test
    fun assertDetailTvShow() {
        Espresso.onView(withId(R.id.tabsDetail)).perform(selectTabAtPosition(1))
        Espresso.onView(withId(R.id.rcyTvShows)).perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                0,
                click()
            )
        )
        Espresso.onView(withId(R.id.lblTitle)).check(ViewAssertions.matches(isDisplayed()))
        Espresso.onView(withId(R.id.lblTotalEpisode)).check(ViewAssertions.matches(isDisplayed()))
    }

    @Test
    fun assertMovieFavourite() {
        Espresso.onView(withId(R.id.rcyMovies)).perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                1,
                click()
            )
        )
        Espresso.onView(withId(R.id.fabFavouriteMovie)).perform(click())
        Espresso.onView(isRoot()).perform(ViewActions.pressBack())
        Espresso.onView(withId(R.id.action_favourite)).perform(click())
        Espresso.onView(isRoot()).perform(waitFor(1000))
        Espresso.onView(withId(R.id.rcyMovies)).perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                0,
                click()
            )
        )
        Espresso.onView(isRoot()).perform(ViewActions.pressBack())
        Espresso.onView(isRoot()).perform(ViewActions.pressBack())
        Espresso.onView(withId(R.id.rcyMovies)).perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                0,
                click()
            )
        )
        Espresso.onView(withId(R.id.fabFavouriteMovie)).perform(click())
        Espresso.onView(isRoot()).perform(ViewActions.pressBack())
        Espresso.onView(withId(R.id.action_favourite)).perform(click())
        Espresso.onView(withId(R.id.rcyMovies)).perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                1,
                click()
            )
        )
        Espresso.onView(withId(R.id.fabFavouriteMovie)).perform(click())
        Espresso.onView(isRoot()).perform(ViewActions.pressBack())
        Espresso.onView(withId(R.id.rcyMovies)).perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                0,
                click()
            )
        )
        Espresso.onView(withId(R.id.fabFavouriteMovie)).perform(click())
        Espresso.onView(isRoot()).perform(ViewActions.pressBack())
        Espresso.onView(withId(R.id.rcyMovies)).check(ViewAssertions.matches(not(isDisplayed())))
    }

    @Test
    fun assertTvShowFavourite() {
        Espresso.onView(withId(R.id.tabsDetail)).perform(selectTabAtPosition(1))
        Espresso.onView(withId(R.id.rcyTvShows)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(1, click()))
        Espresso.onView(withId(R.id.fabFavourite)).perform(click())
        Espresso.onView(isRoot()).perform(ViewActions.pressBack())
        Espresso.onView(withId(R.id.action_favourite)).perform(click())
        Espresso.onView(withId(R.id.tabsDetail)).perform(selectTabAtPosition(1))
        Espresso.onView(isRoot()).perform(waitFor(1000))
        Espresso.onView(withId(R.id.rcyTvShows)).perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(0))
        Espresso.onView(withId(R.id.rcyTvShows)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))
        Espresso.onView(isRoot()).perform(ViewActions.pressBack())
        Espresso.onView(isRoot()).perform(ViewActions.pressBack())
        Espresso.onView(withId(R.id.rcyTvShows)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))
        Espresso.onView(withId(R.id.fabFavourite)).perform(click())
        Espresso.onView(isRoot()).perform(ViewActions.pressBack())
        Espresso.onView(withId(R.id.action_favourite)).perform(click())
        Espresso.onView(withId(R.id.tabsDetail)).perform(selectTabAtPosition(1))
        Espresso.onView(isRoot()).perform(waitFor(1000))
        Espresso.onView(withId(R.id.rcyTvShows)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(1, click()))
        Espresso.onView(withId(R.id.fabFavourite)).perform(click())
        Espresso.onView(isRoot()).perform(ViewActions.pressBack())
        Espresso.onView(withId(R.id.rcyTvShows)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))
        Espresso.onView(withId(R.id.fabFavourite)).perform(click())
        Espresso.onView(isRoot()).perform(ViewActions.pressBack())
        Espresso.onView(withId(R.id.rcyTvShows)).check(ViewAssertions.matches(not(isDisplayed())))

    }

    private fun waitFor(delay: Long): ViewAction? {
        return object : ViewAction {
            override fun getConstraints(): Matcher<View> {
                return isRoot()
            }

            override fun getDescription(): String {
                return "wait for " + delay + "milliseconds"
            }

            override fun perform(uiController: UiController, view: View?) {
                uiController.loopMainThreadForAtLeast(delay)
            }
        }
    }

}