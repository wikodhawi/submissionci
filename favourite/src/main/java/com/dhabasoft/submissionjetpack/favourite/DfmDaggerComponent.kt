package com.dhabasoft.submissionjetpack.favourite

import com.dhabasoft.submissionjetpack.core.di.DynamicFeatureDependencies
import dagger.Component

@Component(dependencies = [DynamicFeatureDependencies::class])
interface DfmDaggerComponent {

    fun inject(activity: FavouriteActivity)

    @Component.Factory
    interface Factory {
        fun create(dependencies: DynamicFeatureDependencies): DfmDaggerComponent
    }
}
